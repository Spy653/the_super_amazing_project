﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerReachEnd : MonoBehaviour 
{
	public LevelManager LevelControl;

	public void Start()
	{
		LevelControl = gameObject.transform.parent.gameObject.transform.parent.gameObject.GetComponent<LevelManager>();
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		bool alldead = false;
		if(other.gameObject.name=="Player")
		{
			
			switch(LevelControl.CurrentLevel)
			{
				case 1: if(LevelControl.Level1EnemyCount == 0){alldead = true;}	break;
				case 2: if(LevelControl.Level1EnemyCount == 0){alldead = true;}	break;
				case 3: if(LevelControl.Level1EnemyCount == 0){alldead = true;}	break;
			}

			if(alldead)
			{		
				if(LevelControl.CurrentLevel==3)
				{
					LevelControl.CurrentLevel = 0;
					alldead = false;
				}
				else
				{
					LevelControl.CurrentLevel++;
					alldead = false;
					other.transform.parent.gameObject.transform.parent.gameObject.transform.position = LevelControl.SpawnPoint.transform.position;
				}
			}
		}
	}
}
