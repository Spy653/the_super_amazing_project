using System.Collections;
using UnityEngine;

public class MenuManager : MonoBehaviour 
{
	public LevelManager LevelControl;

	public void Play() 		
	{
		LevelControl = gameObject.transform.parent.gameObject.transform.parent.gameObject.GetComponent<LevelManager>();
		Debug.Log(LevelControl.gameObject.name);

		if((LevelControl.Level1EnemyCount==0))
		{
			Destroy(LevelControl.LevelsObject);
			LevelControl.GetNewLevelObj();
		}

		LevelControl.CurrentLevel = 1;
		Entity E = LevelControl.PlayerParent.GetComponent<Entity>();
		E.Health = 20;
	}
	public void Close() 	{ Application.Quit(); }
	
}