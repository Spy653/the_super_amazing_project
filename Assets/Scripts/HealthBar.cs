﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour {

	
	public SpriteRenderer 	Renderer;
	public GameObject 		Target;
	private Entity E;
	
	public Sprite[] HealthBarSprites;
	
	void Start()
	{
		E = Target.GetComponent<Entity>();
	}
	
	void Update () 
	{
		if(((20-E.Health)>=0)&&((20-E.Health)<20))
			Renderer.sprite = HealthBarSprites[20-E.Health];
	}
}
