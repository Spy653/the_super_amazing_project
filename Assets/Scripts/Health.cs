﻿using System;

// This class is responsible for managing health, dealing damage, healing, and anything else that comes to mind in future.
public static class Health
{
	public static void Damage(Entity Target, int Damage)
	{
		Target.Health -= Damage;
	}

}
