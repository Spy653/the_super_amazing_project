﻿using UnityEngine;
using UnityEngine.Events;

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class PlayerManager : MonoBehaviour
{
// Variable Declarations
	public 	GameObject 		Parent;	
	public 	GameObject 		Player;
	public 	Entity 	  		Self;
	public 	BoxCollider2D	AttackZone;
	public 	GameObject		Ground;

	public 	int 			Speed;

    public 	Animator        AnimationController;
    public 	Animator        JumpAnimationController;

	private bool 			Sprinting 				= false;

    private float			AttackCoolDown 			= 0;
	private float 			PostAttackCoolDown 		= 0;
	
	public 	List<Entity>	Entities;
 	public  int 			Ei 						= 0;

 	public 	LevelManager	LevelControl;
// Start 	Method
	public void Start()
	{
		Self 					= Parent.GetComponent<Entity>();
	}
// Update 	Method
	public void Update()
	{
		if(Self.Alive)
		{
			Sprinting=false;
			
			if		(Input.GetKey(KeyCode.A))			{ Left();	}
			else if	(Input.GetKey(KeyCode.D))			{ Right();	}
			else   										{ if(PostAttackCoolDown<=0.1f)	{	AnimationController.speed = 0; } }

			if 		(Input.GetKey(KeyCode.K))			{ Attack();	}
			if		(Input.GetKey(KeyCode.W))			{ Jump();	}
			if		(Input.GetKey(KeyCode.LeftControl))	{ Sprint();	}
                        
            if		(AttackCoolDown>0)					{ AttackCoolDown		-= Time.deltaTime;	}
			if		(PostAttackCoolDown>0)				{ PostAttackCoolDown 	-= Time.deltaTime;	}				


			Speed = (Sprinting) ? 5:3 ;
		}
		else
		{
			LevelControl.CurrentLevel = 4;
		}
	}
// Left		Method
	public void Left()
	{
		Parent.transform.position 	+= new Vector3(-(Speed*Time.deltaTime),0,0);
		Player.transform.localScale = new  Vector3(-1.5f,1.5f,1.5f);

		if(PostAttackCoolDown<=0.1f)
		{
            AnimationController.speed = 1;
            AnimationController.Play("PlayerWalk");
        }
    }
// Right 	Method
    public void Right()
	{
		Parent.transform.position 	+= new Vector3((Speed*Time.deltaTime),0,0);		
		Player.transform.localScale = new  Vector3(1.5f,1.5f,1.5f);

		if(PostAttackCoolDown<=0.1f)
        {
            AnimationController.speed = 1;
            AnimationController.Play("PlayerWalk");
        }
    }
// Jump 	Method
    public void Jump() { JumpAnimationController.Play("PlayerJump"); }
// Attack	Method
    public void Attack()
	{
		if(AttackCoolDown<=0)
        {
			AttackCoolDown = 0.3f;
            AnimationController.speed = 1;
            AnimationController.Play("PlayerAttack");

			for(float f = 0;f<=3;f+=Time.deltaTime){}

			foreach(var E in Entities)
			{
				if(E==null)
					continue;
				Health.Damage(E,2);
			}
			PostAttackCoolDown = 0.5f; 
		}
	}
// Sprint 	Method
    public void Sprint() { Sprinting = true; }
}