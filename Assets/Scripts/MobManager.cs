using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;


class MobManager : MonoBehaviour
{
	public 	GameObject 		PlayerParent;
	public 	PlayerManager	PlayerController;
	public 	GameObject 		Player;
	private	Entity 	  		Self;
	private	GameObject		Mob;
	private SpriteRenderer 	MobSpriteRenderer;
	private	BoxCollider2D	AttackZone;

	public 	LevelManager	LevelControl;
	public 	int 			GameLevel;

	private	Animator 		AnimationController;

	public 	float Speed;
	public 	int Level;
	public 	int DirectionOfPlayer = 1;

	private float AttackCoolDown = 0;
	private float PostAttackCoolDown = 0;



	public void Start()
	{
		Mob 				= this.gameObject;
		Self 				= GetComponent<Entity>();
		MobSpriteRenderer 	= GetComponent<SpriteRenderer>();
		AttackZone 			= GetComponent<BoxCollider2D>();
		AnimationController = GetComponent<Animator>();

		AnimationController.speed 		= 0.5f; 

		PlayerParent = PlayerController.Parent;
		Player = PlayerController.Player;
	}
	
	public void Update()
	{
		if(!Self.Alive){
			switch(GameLevel)
			{
				case 1: LevelControl.Level1EnemyCount--; break;
			}
			Mob.active = false;
		}

		float Mx, My, Px, Py;
		Mx = Mob.transform.position.x; 
		Px = Player.transform.position.x;

		float Distance = Vector2.Distance(Mob.transform.position,Player.transform.position);	

		if 	   (Mx > (Px+0.6f))	{	DirectionOfPlayer = -1; }	//Left
		else if(Mx < (Px-0.6f))	{	DirectionOfPlayer = 1; 	}	//Right
		else					{	DirectionOfPlayer = 0;  }	//Center

		if((PostAttackCoolDown<=0.1f)&&(Distance<=7)) { Move(); }

		if((Distance < 1f)&&(PlayerParent.GetComponent<Entity>().Alive)&&(AttackCoolDown<=0))
		{
	        AnimationController.speed = 0.5f;
			AnimationController.Play("Mob"+Level+"Attack");
			Health.Damage(PlayerParent.GetComponent<Entity>(), (Level == 1)?1:2);  
			AttackCoolDown = (Level == 1)?1:2f;
			PostAttackCoolDown = 1;
		}

		AttackCoolDown -= Time.deltaTime;
		PostAttackCoolDown -= Time.deltaTime;
	}

    public void Move()
    {
    	if(DirectionOfPlayer == 0) 	{ goto End;	}

    	Mob.transform.position += 	new Vector3(DirectionOfPlayer*(Speed*Time.deltaTime),0,0);
		Mob.transform.localScale =	new Vector3(DirectionOfPlayer*1.5f,1.5f,1.5f);
        
        AnimationController.speed = 1;
        AnimationController.Play("Mob"+Level+"Walk");

        End:;
    }
} 