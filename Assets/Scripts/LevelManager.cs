﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour 
{
	public GameObject	PlayerParent;

	public GameObject	MenuObject;
	public GameObject	DeathScreen;

	public int 			CurrentLevel 		= 0;

	public int 			Level1EnemyCount;

	public GameObject 	SpawnPoint;

	public GameObject 	Level1Object;

	public GameObject 	LevelsObject;
	public GameObject 	LevelsObjectToClone;

	public Transform[] Objs;	

	void Start()
	{
		GetNewLevelObj();
	}

	public void GetNewLevelObj()
	{
		LevelsObject = Instantiate(LevelsObjectToClone);
		LevelsObject.gameObject.transform.parent = gameObject.transform;

		Objs = LevelsObject.GetComponentsInChildren<Transform>(true);
		foreach(var O in Objs)
		{
			if(O.gameObject.name == "Menu")			{ MenuObject 	= O.gameObject; }
			if(O.gameObject.name == "Level One") 	{ Level1Object 	= O.gameObject; }
			if(O.gameObject.name == "DeathScreen")	{ DeathScreen 	= O.gameObject; }
			if(O.gameObject.name == "Spawn Point")	{ SpawnPoint 	= O.gameObject; }
		}
	}


	void Update () 
	{
		switch(CurrentLevel)
		{
			default: case 0: 
				PlayerParent.active 		= false;
				DeathScreen.active 			= false;
				MenuObject.active 			= true;
				
				LevelsObject.active			= true;
				Level1Object.active 		= false;
			break;
			case 1:	
				PlayerParent.active 		= true;
				DeathScreen.active 			= false;
				MenuObject.active 			= false;
				
				LevelsObject.active			= true;
				Level1Object.active 		= true;
			break;
			case 4:
				PlayerParent.active 		= false;
				DeathScreen.active 			= true;
				MenuObject.active 			= false;
				
				LevelsObject.active			= true;
				Level1Object.active 		= false;
			break;
		}	
	}
}
