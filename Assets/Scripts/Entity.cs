﻿using System;
using UnityEngine;

// This class is how we turn a gameobject into an enemy.
public class Entity : MonoBehaviour
{
	public int Health;
	public string Name;
	public bool Alive = true;
	// Every frame this cycles through and checks your health.
	public void Update()
	{
		if((Health<=0)&&(Alive))
		{
			Debug.Log(Name+" has Died!");
			Alive = false;
			
		}
	}
}
