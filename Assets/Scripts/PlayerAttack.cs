﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayerAttack : MonoBehaviour 
{
	public BoxCollider2D BC;
	public PlayerManager PM;

	// Use this for initialization
	public void Start () {}
	
	// Update is called once per frame
	public void Update () { }

	public void OnTriggerEnter2D(Collider2D Other)
	{
		Entity E = Other.gameObject.GetComponent<Entity>();
		if(!PM.Entities.Contains(E)){ PM.Entities.Add(E);}
		PM.Ei++;
	}

	public void OnTriggerExit2D(Collider2D Other)
	{
		Entity E = Other.gameObject.GetComponent<Entity>();
		if(PM.Entities.Contains(E)){ PM.Entities.Remove(E);}
		PM.Ei--;
	}


}
